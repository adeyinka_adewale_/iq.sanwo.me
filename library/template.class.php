<?php

class Template
{

    protected $variables = array();
    protected $_controller;
    protected $_action;
    protected $_is_admin = 0;
    protected $_is_lone_page = false;
    protected $_title = '';
    protected $_render_custom = false;
    protected $_custom_view = '';
    protected $_qString = '';

    function __construct($controller, $action, $is_admin = 0)
    {
        $this->_controller = $controller;
        $this->_action = $action;
        $this->_is_admin = $is_admin;
    }

    /** Set Variables **/

    function set($name, $value)
    {
        $this->variables[$name] = $value;
    }

    function setTitle($title)
    {
        $this->_title = $title;
    }

    function isLonePage($isLone = false)
    {
        $this->_is_lone_page = $isLone;
    }

    function renderCustom($custom_view = null, $end=false)
    {
        if ($custom_view == null) {
            $this->_render_custom = false;
            $this->_custom_view = '';
        }
        $this->_render_custom = true;
        $this->_custom_view = $custom_view;
        if($end){exit;}
    }

    function setArray($values)
    {
        if (!is_array($values)) {
            return false;
        }
        foreach ($values as $key => $value) {
            $this->variables[$key] = $value;
        }
    }

    function setQString($qString)
    {
        $this->_qString = $qString;
    }

    /*
     * Header type :  if the set to admin  renders the admin Header file
     * */
    function renderAdminHeader($show_admin = false)
    {
        if ($show_admin) {
            $this->_is_admin = 1;
        }
    }

    /** Clean up HTML to remove all unregistered tags **/
    private function cleanHtml($html)
    {
        $dom = new DOMDocument();

        $dom->loadHTML($html);

        $script = $dom->getElementsByTagName('script');

        $remove = array();
        foreach ($script as $item) {
            $remove[] = $item;
        }

        foreach ($remove as $item) {
            $item->parentNode->removeChild($item);
        }

        $html = $dom->saveHTML();
        return $html;
    }

    /** Display Template **/
    function includeView($view)
    {
        //include_once(VIEWS.$view);
    }

    function render($doNotRenderHeader = 0)
    {

        $html = new HTML;
        $myCache = null;
        if (!DEVELOPMENT_ENVIRONMENT) {
            $myCache = new MyPHPCache();
            $myCache->getCurrentPageCache(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php' . $this->_qString);
            $myCache->startViewCache(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php' . $this->_qString);
        }

        $header = ($this->_is_admin == 0 ? 'header.php' : 'admin_header.php');
        global $allowedJS;
        global $allowedCSS;
        extract($this->variables);


        if ($doNotRenderHeader == 0) {

            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $header)) {
                include(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $header);
            } else {
                include(ROOT . DS . 'application' . DS . 'views' . DS . $header);
            }
        }
        if ($this->_render_custom && strlen($this->_custom_view) > 0) {
            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_custom_view . '.php')) {
                include(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_custom_view . '.php');
            }else if(file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_custom_view . '.php')){
                include(ROOT . DS . 'application' . DS . 'views' . DS . $this->_custom_view . '.php');
            }
        } else {
            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php')) {
                include(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php');
            }
        }

        if ($doNotRenderHeader == 0) {
            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php')) {
                include(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php');
            } else {
                include(ROOT . DS . 'application' . DS . 'views' . DS . 'footer.php');
            }
        }
        if (!DEVELOPMENT_ENVIRONMENT && $myCache != null) {
            $myCache->endViewCache();
        }

    }

}