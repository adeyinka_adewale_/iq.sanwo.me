<?php


namespace SanwoPHPAdapter;


use SanwoPHPAdapter\Globals\ServiceConstant;

class Merchant_transactionsAdapter extends BaseAdapter
{

    /**
     * @author Ita Ukemeabasi
     * @param $name
     * @param $address
     * @param $phone_number
     * @param $business_type
     * @param $charge_type
     * @param $charge
     * @param $account_number
     * @param $bank
     * @param $account_name
     * @param $created_by
     * @param $email
     * @param $password
     * @return array|mixed|string
     */
    public function Merchant_transactionsAdapter($name, $address, $phone_number, $business_type, $charge_type, $charge, $account_number, $bank, $account_name, $created_by, $email, $password)
    {
        return $this->request(
            "merchant_transactions/addMerchant_transactions/",
            array(
                'name' => $name,
                'address' => $address,
                'phone_number' => $phone_number,
                'business_type' => $business_type,
                'charge_type' => $charge_type,
                'charge' => $charge,
                'account_number' => $account_number,
                'bank' => $bank,
                'account_name' => $account_name,
                'created_by' => $created_by,
                'email' => $email,
                'password' => $password,
            ),
            self::HTTP_POST
        );
    }
}