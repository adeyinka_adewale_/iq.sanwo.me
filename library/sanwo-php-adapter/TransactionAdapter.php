<?php


namespace SanwoPHPAdapter;


class TransactionAdapter extends BaseAdapter {

    public function getTransaction($id,$type,$offset,$count){
        switch($type){
            case 2:
                return $this->request(
                    "creditTransactions/getAll",
                    array(
                        'merchant_id' => $id,
                        'device_type_id' => $type,
                        'count' => $count,
                        'offset' => $offset,
                        'with_card'=>1
                    ),
                    self::HTTP_GET
                );
                break;
            case 1:
                return $this->request(
                    "creditTransactions/getAll",
                    array(
                        'agent_id' => $id,
                        'count' => $count,
                        'offset' => $offset,
                        'with_card'=>1
                    ),
                    self::HTTP_GET
                );
                break;
            case 3:
                return $this->request(
                    "creditTransactions/getAll",
                    array(
                        'serial_number' => $id,
                        'count' => $count,
                        'device_type_id' => 2,
                        'with_merchant' => 1,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
            case 4:
                return $this->request(
                    "creditTransactions/getcardstats",
                    array(
                        'count' => $count,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
            case 5:
                return $this->getCardTransactions($id, $count, $offset);
                break;
            case 6:
                return $this->request(
                    "creditTransactions/getAll",
                    array(
                        'serial_number' => $id,
                        'count' => $count,
                        'device_type_id' => 1,
                        'with_agent' => 1,
                        'offset' => $offset
                    ),
                    self::HTTP_GET
                );
                break;
        }
    }
    public function getCardTransaction($serial_number,$offset,$count){
        //getTransactedMerchant
        return $this->request(
            "creditTransactions/getTransactedMerchant",
            array(
                'serial_number' => $serial_number,
                'count' => $count,
                'offset' => $offset
            ),
            self::HTTP_GET
        );
    }


    public function getMerchantTransaction($id, $count, $offset, $type, $check_history_id){

        return $this->request(
                    "creditTransactions/getAll",
                    array(
                        'merchant_id' => $id,
                        'device_type_id' => $type,
                        'count' => $count,
                        'offset' => $offset,
                        // 'with_card'=>1,
                        // 'wtih_holder' =>1,
                        'check_history_id' => $check_history_id
                    ),
                    self::HTTP_GET
                );
    }



    public function getMerchantSyncSummary($offset, $count){
         return $this->request(
                    "checkhistory/fetchAll",
                    array(
                        'offset' => $offset,
                        'count' => $count,
                        'with_merchant' => 1
                        
                    ),
                    self::HTTP_GET
                );
    }

    //get agent transaction
    public function getAgentTransactions($id, $count, $offset){
        return $this->request(
                    "credittransaction/getall",
                    array(
                        'agent_id' => $id,
                        'count' => $count,
                        'offset' => $offset,
                        'device_type_id' => 1
                    ),
                    self::HTTP_GET
                );
    }
} 