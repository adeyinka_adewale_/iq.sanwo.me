<?php
/**
 * Created by PhpStorm.
 * User: epapa
 * Date: 5/2/15
 * Time: 5:42 AM
 */

namespace SanwoPHPAdapter;


class AdminAdapter extends BaseAdapter
{
    /**
     * @author Adegoke Obasa
     * @param $email
     * @param $password
     * @param $user_type_id
     * @param $status
     * @return array|mixed|string
     */
    public function registerUser($email, $password, $user_type_id,$telephone,$firstname,$lastname,$middlename,$gender,$address, $status)
    {
        return $this->request(
            "admin/registerUser/",
            array(
                'email' => $email,
                'password' => $password,
                'telephone' => $telephone,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'middlename' => $middlename,
                'gender' => $gender,
                'address' => $address,
                'user_type_id' => $user_type_id,
                'status' => $status
            ),
            self::HTTP_POST
        );
    }

    /**
     * @author Adegoke Obasa
     * @param $user_id
     * @param $status
     * @return array|mixed|string
     */
    public function changeUserStatus($user_id, $status)
    {
        return $this->request(
            "admin/changeUserStatus/",
            array(
                'user_id' => $user_id,
                'status' => $status
            ),
            self::HTTP_POST
        );
    }
}