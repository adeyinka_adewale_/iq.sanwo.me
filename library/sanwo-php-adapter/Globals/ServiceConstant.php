<?php
namespace SanwoPHPAdapter\Globals;

class ServiceConstant {
    const STATUS_ACTIVE = 1;
    const STATUS_REMOVED = 2;
    const STATUS_BLOCKED = 3;
    const STATUS_DEVICE_UNASSIGNED = 4;
    const STATUS_DEVICE_ASSIGNED = 5;
    const STATUS_DEVICE_ONLINE = 6;
    const STATUS_DEVICE_OFFLINE = 7;
    const STATUS_TRANS_SUCCESSFUL = 8;
    const STATUS_TRANS_FAILED = 9;
    const STATUS_CARD_UNREGISTERED = 10;
    const STATUS_CARD_REGISTERED = 11;
    const STATUS_CARD_BLOCKED = 12;
    const STATUS_CARD_DEACTIVATED = 13;
    const STATUS_CARD_EXPIRED = 14;
    const STATUS_INACTIVE = 15;

    const BUSINESS_TYPE_TRANSPORT = 1;
    const BUSINESS_TYPE_EATERY = 2;
    const BUSINESS_TYPE_RETAIL = 3;

    const CARD_TYPE_PRODUCTION = 1;
    const CARD_TYPE_DEV = 2;

    const CHARGE_TYPE_ABSOLUTE = 1;
    const CHARGE_TYPE_PERCENTAGE = 2;

    const DEVICE_TYPE_AGENT = 1;
    const DEVICE_TYPE_CASHIER = 2;

    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_MANAGER = 2;
    const USER_TYPE_CASHIER = 3;
    const USER_TYPE_AGENT = 4;
    const USER_TYPE_CUSTOMER = 5;
    const USER_TYPE_SUPPORT = 6;
    const USER_TYPE_ACCOUNTANT = 7;
    const USER_TYPE_SUPER_ADMIN = 8;
    const USER_TYPE_ISSUER_USER = 9;
    const USER_TYPE_ISSUER_ADMIN = 10;


    const URL_ADD_CASHIER = 'merchant/addcashier/';
    const URL_GET_ALL_CASHIER = 'merchant/getcashiers/';
    const URL_ASSIGN_DEVICE = 'device/assigntomerchant/';
    const URL_GET_MERCHANT_DEVICES = 'merchant/getdevices/';
    const URL_GET_PAGE_SUMMARY = 'report/total/';
    const URL_CREATE_AGENT = 'admin/registerUser/';
    const URL_CREATE_CUSTOMER = 'customer/register/';
    const URL_GET_USER = 'user/get/';
    const URL_GET_AGENT_DEVICES = 'agent/getdevice/';
    const URL_CREDIT_AGENT = 'topuptransaction/add/';
    const URL_GET_AGENT = 'agent/getAll/';
    const URL_ADD_ISSUER = 'issuer/add/';
    const URL_ADD_ISSUER_USER = 'issuer/adduser/';
    const URL_ADD_GET_All = 'issuer/getAll/';
    const URL_ADD_GET_USERS = 'issuer/getUsers/';
    const URL_CHANGE_STATUS = 'issuer/changeStatus/';
    const URL_GET_ISSUER_USERS= 'issuer/getusers';


    public static function getStatus($status)
    {
        switch($status)
        {
            case 1:
                return 'ACTIVE';
            case 2:
               return 'REMOVED';
            case 3:
                return 'BLOCKED';
            case 4:
                return 'DEVICE UNASSIGNED';
            case 5:
                return 'DEVICE ASSIGNED';
            case 6:
                return 'DEVICE ONLINE';
            case 7:
                return 'DEVICE OFFLINE';
            case 8:
                return 'TRANS SUCCESSFUL';
            case 9:
                return 'TRANS FAILED';
            case 10:
                return 'CARD UNREGISTERED';
            case 11:
                return 'CARD REGISTERED';
            case 12:
                return 'CARD BLOCKED';
            case 13:
                return 'CARD DEACTIVATED';
            case 14:
                return 'CARD EXPIRED';
            case 15:
                return 'STATUS INACTIVE';

        }
        return '';
    }
}