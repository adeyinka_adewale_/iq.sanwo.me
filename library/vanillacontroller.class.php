<?php

class VanillaController {
	
	protected $_controller;
	protected $_action;
	protected $_template;

	public $doNotRenderHeader;
    public $is_admin = 0;
	public $render;
	public $qString;
	public $title = PROJECT_NAME;
    public $page_menus = array();

    function beforeAction(){
        if(!$this->validateHeaders()) {
//            Calypso::getInstance()->sendErrorJSON("Access Denied");
//            exit;
            Calypso::getInstance()->AppRedirect('home','index');
        }
    }

    function afterAction(){

    }

	function __construct($controller, $action) {
		
		global $inflect;

		$this->_controller = ucfirst($controller);
		$this->_action = $action;
		$model = ucfirst($inflect->singularize($controller));
		$this->doNotRenderHeader = 0;
		$this->render = 1;
		$this->$model = new $model;
		$this->_template = new Template($controller,$action,$this->is_admin);

	}

    function setTitle($title){
        $this->title = $title;
        $this->_template->setTitle($this->title);
    }

    function setIsLonePage($isLone){
        $this->_template->isLonePage($isLone);
    }
    public function validateData(array $data, $requiredFields, $returnResponseAsJson = true) {
        if(empty($data)) {
            if($returnResponseAsJson){
                Calypso::getInstance()->sendErrorJSON( 'Required fields not sent');
            }else {
                return false;
            }
        }
        foreach($requiredFields as $v) {
            if (!array_key_exists($v, $data) || trim($data[$v])=='') {
                if($returnResponseAsJson) {
                    Calypso::getInstance()->sendErrorJSON('Not all required fields are sent. #Reason: '.$v.' not found');
                }else {
                    return false;
                }
            }
        }
        return true;
    }

    public function isAuthenticated(){
        if(Calypso::getInstance()->session('credentials')){
            return true;
        }
        return false;
    }
    public function validateForm(){
        if(!isset(Calypso::getInstance()->post()->_csrf))
            return false;
        $v = Calypso::getInstance()->post()->_csrf;
        if($v != Calypso::getInstance()->session('_csrf'))
            return false;

        return true;
    }
    public function csrf(){
        Calypso::getInstance()->session('_csrf',BasicEncrypt::doOneWayEncrypt(mt_rand(10,99999999999).'$$'.date('Y-m-d h:s')));
    }
    public function isAdmin(){
        if(!$this->isAuthenticated()){
            return false;
        }
        $session = Calypso::getInstance()->session('user');
        return $session[0]['User']['type']==ROLE_ADMIN;
    }
    public function isAuthorized($controller){
        $roles = Calypso::getInstance()->session('roles');
        $authorized = false;
        foreach($roles as $role){
            if($role['Module']['system_name'] == $controller){
                $authorized = true;
                break;
            }
        }
        return $authorized;
    }
    function setQString($qString){
        $this->_template->setQString($qString);
    }
	function set($name,$value) {
		$this->_template->set($name,$value);
	}
    function renderCustom($custom_view=null,$end=false){
        $this->_template->renderCustom($custom_view,$end);
    }
    function setArray($data) {
		$this->_template->setArray($data);
	}

    function setAsAdmin(){
        $this->_template->renderAdminHeader(true);
    }
    function removeAsAdmin(){
        $this->_template->renderAdminHeader(false);
    }

	function __destruct() {
		if ($this->render) {
			$this->_template->render($this->doNotRenderHeader);
		}
	}

    function validateHeaders(){
        $headers = apache_request_headers();
        if(Calypso::getInstance()->isLoggedIn()) {
            return true;
        }
        if (!isset($headers['_service_key'],$headers['_service_auth_token'])){
            return false;
        }
        /**if(empty(Calypso::getInstance()->session('_authToken'))) {
            return false;
        }**/
        if(Calypso::getInstance()->session('_authToken') != $headers['_service_auth_token']) {
            return false;
        }
        return true;
    }

    function logIP($id, $userObject) {
        $userObject->id = $id;
        $userObject->last_login_ip = $_SERVER['REMOTE_ADDR'];
        $userObject->last_login_date = date('Y-m-d H:i:s');
        $userObject->save();
    }

    function sendAsyncResponse($payload, $callback) {
        exit("<script>window.top.receiveAsyncResponse(".json_encode(['data'=>$payload, 'callback'=>$callback]).");</script>");
    }
}