<?php
class CalpysoMailer {

    private static $instance = null;
    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new CalpysoMailer();
        }
        return self::$instance;
    }
    public function __construct(){}
    public function sendEmails($payload){
        //return true;
        if($payload instanceof CalypsoMailObject){
            date_default_timezone_set('Etc/UTC');

            require_once('PHPMailer/PHPMailerAutoload.php');
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 1;
            $mail->Debugoutput = 'html';
            $mail->Host = "smtp.gmail.com";
            $mail->Username = "xmailerdev@gmail.com";
            $mail->Password = "XmailerDev!";
            $mail->SMTPSecure = 'tls';
            //XmailerDev! | xmailerdev@gmail.com
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail-> SingleTo = true;

            $mail->From = $payload->from;
            $mail->FromName = $payload->from_name;

            //$mail->setFrom($payload->from, $payload->from_name);
            $mail->addAddress($payload->emails, $payload->emails);
            $mail->Subject = $payload->subject;
            $mail->Body = $payload->message;
            if($payload->is_HTML){
                $mail->isHTML($payload->is_HTML);
                $mail->msgHTML($payload->message, dirname(__FILE__));
            }else{
                $mail->Body = $payload->message;
            }
            //$mail->addAttachment('images/phpmailer_mini.png');
            //Debug
            if (!$mail->send()) {
                //echo "Mailer Error: " . $mail->ErrorInfo;
                $payload->message = $mail->ErrorInfo;
                $this->Log(new LogData($payload->identifier,$payload,$mail->ErrorInfo));
                return false;
            } else {
                //echo "Message sent!";
                return true;
            }
            //return $mail->send();
        }else{
            //throw new Exception("Error. Instance of CalypsoMailObject expected");
            $this->Log(new LogData($payload->identifier,$payload,'Error. Instance of CalypsoMailObject expected'));
            return false;
        }
    }
    public function Log($data,$log_name=NULL,$ext='.xlog'){
        if(!$log_name){
            $log_name = date('Y-m-d-H');
        }

        $f= fopen(LOG_FILE_PATH.$log_name.$ext,'a');
        fwrite($f,serialize($data)."\n");
        fclose($f);

    }

    public function readLog($log){
        $d = unserialize($log);
        return $d;
    }
    public function getLogs($ext='.xlog'){
        $dir = scandir(LOG_FILE_PATH);
        $logs = array();
        if(is_array($dir) && count($dir) > 2){
            for($i = 2; $i < count($dir); $i++){
                if(substr($dir[$i],strpos($dir[$i],'.'),strlen($dir[$i]))==$ext){
                    array_push($logs,$dir[$i]);
                }
            }
        }
        return $logs;
    }
}

class CalypsoMailObject {
    public $identifier = null;
    public $emails;
    public $message;
    public $from;
    public $from_name;
    public $subject;
    public $is_HTML = true;

    public function __construct(){}
}
class LogData{
    public $id = null;
    public $payload = null;
    public $info = '';

    public function __construct($task_id,$pay_load,$info){
        $this->id = $task_id; $this->payload = $pay_load;
        $this->info = $info;
        return $this;
    }
}