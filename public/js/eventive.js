var Eventive = {};
var _w = window;
var _b = window.document.body;




Eventive.components = {
    searchFilter: {
        show:function(){

        },
        hide:function(){},
        hang:function(){
            if(_b.scrollTop < 70) {
                $(".floating").removeClass('filter_float');
            }else {
                $(".floating").addClass('filter_float');
            }
        }
    }
}

Eventive.AjaxCallbacks = {
    eventCreationModal:function(data) {
        var statusBx = $('#create_event_status');
        if(data.status == 'error')
        {
            var st = typeof data.message != 'undefined' ? data.message : "<strong>Oooops!</strong> Event not created. Please be sure you have all the fields filled";
            statusBx.html(st);
            statusBx.removeClass('alert-success').addClass('alert-danger').removeClass('hidden');
        }else {
            var st = typeof data.message != 'undefined' ? data.message : "<strong>Success!</strong> Please wait...";
            statusBx.html(st);
            statusBx.removeClass('alert-danger').addClass('alert-success').removeClass('hidden');
            setTimeout(function(){window.location.reload()},3000);
        }
    },
    onEventBannerUploaded:function(data){
        if(data.status=='success') {
            window.location.reload();
        }else {
            alert(data.message);
        }
    }
}

Eventive.Ajax = {
    fetchView:function(page, url, callback){
        $(page).load(url,function(html){
            if(typeof callback == 'function')
            {
                callback(html);
            }
        });
    },
    init:function(){
        $("#age_restriction").unbind('change').on('change', function(){
            if($(this).is(':checked')) {
                $("#age_bracket").removeClass('hidden');
            }else {
                $("#age_bracket").addClass('hidden');
            }
        });

        $('.ajax-form').each(function(v){
            $(v).prop('target','ajax_form_frame');
        });

        $(".ajax-page").unbind('click').on('click', function(){
            $(".ajax-page").removeClass('event-menu-active').addClass('btn-default');
            $(this).addClass('event-menu-active').removeClass('btn-default');
            Eventive.Ajax.fetchView("#main-ajax-page", $(this).data('url'), function(resp){
                Eventive.Ajax.init();
            });
        });

        $('.choose-image').unbind('click').on('click', function(){
            $("#file-selector").trigger('click').on('change', function(e){
                console.log(e.target.files);
                if(typeof e.target.files != 'undefined')
                {
                    $("#event-banner").submit();
                    $("#eventBannerModal").modal('show');
                }
            });
        });
        var now = new Date();
        var picker = $(".datepicker").datepicker({
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > picker.date.valueOf()) {
                var newDate = new Date(ev.date)
                //newDate.setDate(newDate.getDate() + 1);
                //picker.setValue(newDate);
            }
            picker.hide();
        }).data('datepicker');
    }
}


$(document).ready(function(){
    Eventive.Ajax.init();
});

_w.addEventListener('scroll',function(e){
    //console.log(_b.scrollTop);
    //console.log(e.target.body.scrollHeight);
    Eventive.components.searchFilter.hang();
},false);