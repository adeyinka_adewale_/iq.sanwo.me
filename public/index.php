<?php	

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('APPLICATION', ROOT.DS.'application');
define('VIEWS', APPLICATION.DS.'views'.DS);
define('CONTROLLERS', APPLICATION.DS.'controllers'.DS);
define('MODELS', APPLICATION.DS.'models'.DS);

$url = (isset($_GET['url'])?$_GET['url']:null);

require_once (ROOT . DS . 'library' . DS . 'bootstrap.php');

