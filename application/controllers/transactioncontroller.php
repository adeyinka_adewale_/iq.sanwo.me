<?php

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;


class TransactionController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){
    	$data = Calypso::getInstance()->session('user');

        //create Transaction comes here
         $postData = Calypso::getInstance()->post(true);

         
         if (!empty($postData)){
            $postData['user_type_id'] = 4;
            $postData['status'] = 1;
            var_dump($postData);

            $check =$this->validateData($postData, ['firstname', 'lastname', 'email', 'telephone',
             'gender', 'user_type_id', 'status']);

            if($check){
                $Transaction = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $request = $Transaction->createTransaction(
                        $postData['email'],
                        $postData['telephone'],
                        $postData['password'],
                        $postData['user_type_id'],
                        $postData['status']       
                    );

                $response = new ResponseHandler($request);
                if($response->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Transaction created successfully!');
                }else{

                    Calypso::getInstance()->setFlashErrorMsg($response->getError());
                }
             //   Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
                //var_dump($response);

            }


         }

        $issuerAdapter = new IssuerAdapter($data['id'], RequestHelper::getAccessToken());

        $trans_data = $issuerAdapter->getIssuerUsers(16, 4);


        //var_dump($trans_data);


        
        $trans_data = new ResponseHandler($trans_data);

        //var_dump($trans_data);
        if($trans_data->getStatus() == ResponseHandler::STATUS_OK)
        {
            $TransactionData = $trans_data->getData();
            Calypso::getInstance()->session('Transactions', $TransactionData);
            $this->set('Transactions', $TransactionData);
        }
    }
}

?>