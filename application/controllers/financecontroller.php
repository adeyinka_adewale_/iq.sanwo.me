<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/26/2015
 * Time: 5:05 AM
 */
class FinanceController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){}
}