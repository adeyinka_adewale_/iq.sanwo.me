<?php

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;


class AgentController extends VanillaController
{
    private $noAuth = [];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }

    public function index(){
        $data = Calypso::getInstance()->session('user');

        //create agent comes here
         $postData = Calypso::getInstance()->post(true);

         
         if (!empty($postData)){
            $postData['user_type_id'] = 4;
            $postData['status'] = 1;
            //var_dump($postData);

            $check =$this->validateData($postData, ['firstname', 'lastname', 'email', 'telephone',
             'gender', 'user_type_id', 'status']);

            if($check){
                $agent = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $request = $agent->createAgent(
                        $postData['email'],
                        $postData['telephone'],
                        $postData['password'],
                        $postData['user_type_id'],
                        $postData['status']       
                    );

                $response = new ResponseHandler($request);
                if($response->getStatus() == ResponseHandler::STATUS_OK){
                    Calypso::getInstance()->setFlashSuccessMsg('Agent created successfully!');
                }else{

                    Calypso::getInstance()->setFlashErrorMsg($response->getError());
                }
             //   Calypso::getInstance()->unsetSession('merchants'); // Clear cached list
                //var_dump($response);

            }


         }
         $agentUsers = new AgentAdapter($data['id'], RequestHelper::getAccessToken());
         $response = $agentUsers->getAllAgents();
         $response = new ResponseHandler($response);

        //echo json_encode($response->getData());
         if($response->getStatus() == ResponseHandler::STATUS_OK)
         {
            $this->set('Agents', $response->getData());
         }
         else
            Calypso::getInstance()->setFlashErrorMsg('Error loading agents');
         

        //  //get all devices issuer has
        //  $agentUsers = new UserAdapter($data['id'], RequestHelper::getAccessToken());
        // // $response = $agentUsers->


        //  $allDevices = new DeviceAdapter($data['id'], RequestHelper::getAccessToken());
        //  $response = $allDevices->getAll();

        //  $reponse = new ResponseHandler($response);

        //  var_dump($response);
            
    }

    public function transaction($agent_id){

        $data = Calypso::getInstance()->session('user');
        if (!$agent_id){
            Calypso::getInstance()->setFlashErrorMsg('Invalid Agent Selected!');
        }

        $transaction_adpater = new TransactionAdapter($data['id'], RequestHelper::getAccessToken());

        $agent_transaction = $transaction_adpater->getAgentTransactions($agent_id, 100,0);
        $result = new ResponseHandler($agent_transaction);
      
        //echo json_encode($result->getData());
        if ($result->getStatus() == ResponseHandler::STATUS_OK){
            $this->set('transactions', $result->getData());
        }
        else{
            Calypso::getInstance()->setFlashErrorMsg('Error loading transactions');
        }
    }

    public function history($agent_id){
        $data = Calypso::getInstance()->session('user');
        if (!$agent_id){
              Calypso::getInstance()->setFlashErrorMsg('Invalid Agent Selected!');
        }

        $topup_adapter = new TopupTransactionAdapter($data['id'], RequestHelper::getAccessToken());
        $agent_history = $topup_adapter->getHistory($agent_id);
        $result = new ResponseHandler($agent_history);

        if($result->getStatus() == ResponseHandler::STATUS_OK){
            $this->set('transactions', $result->getData());
        }
        else
            Calypso::getInstance()->setFlashErrorMsg('Error loading Vendors credit history');

        
    }
}

?>