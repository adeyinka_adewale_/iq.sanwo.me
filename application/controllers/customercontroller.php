<?php

use SanwoPHPAdapter\UserAdapter;
use SanwoPHPAdapter\Util\Response;
use SanwoPHPAdapter\ResponseHandler;
use SanwoPHPAdapter\TransactionAdapter;
use SanwoPHPAdapter\AgentAdapter;
use SanwoPHPAdapter\RequestHelper;
use SanwoPHPAdapter\DeviceAdapter;
use SanwoPHPAdapter\CardAdapter;
use SanwoPHPAdapter\CustomerAdapter;
use SanwoPHPAdapter\IssuerAdapter;
use SanwoPHPAdapter\MerchantAdapter;
use SanwoPHPAdapter\CashierAdapter;
use SanwoPHPAdapter\TopupTransactionAdapter;
use SanwoPHPAdapter\SettingsAdapter;
use SanwoPHPAdapter\Globals\ServiceConstant;

class CustomerController extends VanillaController{

    private $noAuth = ['index'];
    public function beforeAction() {
        if(in_array($this->_action, $this->noAuth)) {
            return true;
        }
        parent::beforeAction();
    }



    public function index(){
    	$postData = Calypso::getInstance()->post(true);
        $data = Calypso::getInstance()->session('user');
        if(!empty($postData))
        {
            if($this->validateData($postData,['telephone','firstname','card_serial','lastname','address']))
            {
                //Create a customer
                var_dump($postData);
                $createUserAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
                $response = $createUserAdp->createCustomer(
                    $postData['card_serial'],$postData['email'],$postData['telephone'], $postData['firstname'],$postData['lastname'],$postData['middlename'],$postData['gender'],$postData['address'],5,1
                );

                $handler = new ResponseHandler($response);


                if($handler->getStatus() == ResponseHandler::STATUS_OK){

                    Calypso::getInstance()->setFlashSuccessMsg('Customer registered successfully');
                }else{
                    Calypso::getInstance()->setFlashErrorMsg($handler->getError());
                }
            }else{
                Calypso::getInstance()->setFlashErrorMsg('Please ensure you fill telephone, first name, last name,address, and the BeepPay serial no fields');
            }

        }
        //Change this API to issuerusers/getall
        $userAdp = new UserAdapter($data['id'], RequestHelper::getAccessToken());
        $users_data = $userAdp->getAllByType(5,0,10000);

        $users_data = new ResponseHandler($users_data);
          echo json_encode($users_data->getData());
        if($users_data->getStatus() == ResponseHandler::STATUS_OK)
        {
            $usersData = $users_data->getData();
            $this->set('customers', $usersData);
        }
    }


    public function details($id){
        $postData = Calypso::getInstance()->post(true);
        $data = Calypso::getInstance()->session('user');

        //fetch all the cards a customer has
        $customerAdapter = new CustomerAdapter($data['id'], RequestHelper::getAccessToken());

        $request = $customerAdapter->getCards($id, null, null);

        $response = new ResponseHandler($request);
        $this->set('details', $response);
        // var_dump($response);
    }




    }


?>