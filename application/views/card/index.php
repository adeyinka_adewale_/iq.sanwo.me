<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Registered Cards</h1>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?>&nbsp;Cards
        </h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Serial Number</th>
        <th tabindex="0" rowspan="1" colspan="1">Batch Number</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Ref</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Current Balance</th>
        <th tabindex="0" rowspan="1" colspan="1">Registration Date</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Status</th>
        <th tabindex="0" rowspan="1" colspan="1">Action</th>
    </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($cards)) {
                $x = 0;
                foreach($cards as $card){
                    ?>
                    <tr>
                        <td><?php echo ++$x; ?></td>
                        <td><?php echo $card['serial_number']; ?></td>
                        <td><?php echo $card['batch_number']; ?></td>
                        <td><?php echo $card['ref_number']; ?></td>
                        <td><?php echo $card['balance']; ?></td>
                        <td><?php echo $card['created_time']; ?></td>
                        <td><?php echo \SanwoPHPAdapter\Globals\ServiceConstant::getStatus($card['status']); ?></td>
                        <td><a class="btn btn-sm btn-primary" href="<?php echo BASE_PATH; ?>/card/customercontributionhistory/<?php echo $card['serial_number'] ?>/<?php echo $card['id'] ?>">Transactions</a></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
