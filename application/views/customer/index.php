<?php
Calypso::AddPartialView('_addCustomerModal');
Calypso::AddPartialView('notices');
Calypso::AddPartialView('_customerDeactivate');
?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Registered Customers
<a href="<?php echo BASE_PATH; ?>/customer" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</h1>
<button type="button" class="btn btn-success btn-md pull-right evt-margin-top-x50-neg" data-toggle="modal" data-target="#add_customer">Add New Customer</button>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?> Customers</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
            <tr role="row">
                <th tabindex="0" rowspan="1" colspan="1">#</th>
                <th tabindex="0" rowspan="1" colspan="1">Name</th>
                <th tabindex="0" rowspan="1" colspan="1">Phone</th>
                <th tabindex="0" rowspan="1" colspan="1">Plate No</th>
                <th tabindex="0" rowspan="1" colspan="1">Vehicle Color</th>
                <th tabindex="0" rowspan="1" colspan="1">Vehicle Brand</th>
                <th tabindex="0" rowspan="1" colspan="1">Reg. Date</th>
                <th tabindex="0" rowspan="1" colspan="1">Action</th>
            </tr>
            </thead>
            <tbody>
        <?php
        if(!empty($customers)) {
            $x = 0;
            foreach ($customers as $customer) {

                ?>

                <tr>
                    <td><?php echo ++$x; ?></td>
                    <td><?php echo ucwords($customer['profile']['firstname']) . ' ' . ucwords($customer['profile']['middlename']) . ' ' . strtoupper($customer['profile']['lastname']) ?></td>
                    
                    <td><?php echo $customer['telephone'] ?></td>
                    <td>KJR 235 YT</td>
                    <td>Red</td>
                    <td>Toyota</td>
                    <td><?php echo $customer['created_date'] ?></td>
                    <td>
                        <a class="btn btn-info btn-xs customer_detail" data-id="<?php echo $customer['id'] ?>" href="<?php echo BASE_PATH; ?>/customer/details/<?php echo $customer['id'] ?>"
                         >transaction</a>  
                         <a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalDeactivate">Deactivate</a> 
                    </td>
                </tr>
                <?php
            }
        }
        ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
<?php
Calypso::AddPartialView('_customerDetailModal');
Calypso::AddPartialView('_addSavingsCardModal');
Calypso::AddPartialView('_addCustomerCycleModal');
?>
<script type="text/javascript">
    var BASE_PATH = "<?php echo BASE_PATH; ?>";
    $(document).ready(function(){
        $("#customer_detail").unbind('click').on('click', function(){

        });
        $("#customer_detail_modal").on('shown.bs.modal', function(event){

            var button = $(event.relatedTarget) // Button that triggered the modal
            $.ajax({url: BASE_PATH+"/public/home/customerdetail/"+button.data('id'), success: function(result){
                $("#customer_detail_body").html(result);
            }});
        }).on('hidden.bs.modal', function(e){
            $("#customer_detail_body").html('Loading...');
        });


        $('table').DataTable();
    });

</script>
