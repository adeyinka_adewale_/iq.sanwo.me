<?php
Calypso::AddPartialView('notices');
if(!empty($details)){
    //var_dump($details);
}
?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-primary" href="#trans">
    View Transactions
    </a>
    </div>
</div>
<div class="row">             
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Customer Details</h5>
            </div>
            <div class="ibox-content">
                 <div id="editable_wrapper" class="dataTables_wrapper form-inline" style="padding-bottom:0px;">                 
                <table class="table table-bordered" style="margin-bottom:0px;">
                    <thead>
                    <tr>
                        <th>Customer name</th>
                        <th>George Wilson</th>
                    </tr>
                    <tr>
                        <th>Customer Plate Number</th>
                        <th>KJH 483 JS</th>
                    </tr>
                    <tr>
                        <th>Customer Address</th>
                        <th>Sabo, Abeokuta, Ogun State</th>
                    </tr>
                    <tr>
                        <th>Customer Phone Number</th>
                        <th>08362627383</th>
                    </tr>
                    <tr>
                        <th>Customer Card Serial</th>
                        <th>KJS939303</th>
                    </tr>
                    <tr>
                        <th>Customer Card Balance</th>
                        <th>&#8358;&nbsp;850</th>
                    </tr>

                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Registered Vehicles</h5>
            </div>
            <div class="ibox-content">
                 <div id="editable_wrapper" class="dataTables_wrapper form-inline" style="padding-bottom:0px;"> 
                <table id="__devices" class="table table-bordered" style="margin-bottom:0px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Plate Number</th>
                        <th>Vehicle Color</th>
                        <th>Vehicle Brand</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>KJA 352 SD</td>
                            <td>Red</td>
                            <td>Toyota</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>JKE 737 RE</td>
                            <td>Black</td>
                            <td>Honda</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>List of Transactions (From Most Recent)</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" id="trans">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">Plate Number</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Balance</th>
        <th tabindex="0" rowspan="1" colspan="1">Time Purchased</th>        
        <th tabindex="0" rowspan="1" colspan="1">Trans. Time</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync. Time</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>KJA 352 SD</td>
            <td>850</td>
            <td>4 hours</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
            <td>KJA 352 SD</td>
            <td>1250</td>
            <td>4 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
        <tr>
            <td>KJA 352 SD</td>
            <td>850</td>
            <td>4 hours</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
            <td>KJA 352 SD</td>
            <td>1250</td>
            <td>4 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
        <tr>
            <td>KJA 352 SD</td>
            <td>850</td>
            <td>4 hours</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
            <td>KJA 352 SD</td>
            <td>1250</td>
            <td>4 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
    </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
