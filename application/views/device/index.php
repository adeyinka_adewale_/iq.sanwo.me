<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Registered Devices</h1>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?> Devices</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Device ID</th>
        <th tabindex="0" rowspan="1" colspan="1">Device Serial</th>
        <th tabindex="0" rowspan="1" colspan="1">Device Site</th>
        <th tabindex="0" rowspan="1" colspan="1">Device Status</th>
        <th tabindex="0" rowspan="1" colspan="1">Action</th>
    </tr>
    </thead>
    <tbody>
            <?php
            if(!empty($devices)) {
                $x = 0;
                foreach($devices as $device){
                ?>
                <tr>
                    <td><?php echo ++$x; ?></td>
                   <td><?php echo 'D-0'.$device['id'] ?></td>
                    <td><a href="/home/device/<?php echo $device['id']; ?>"><?php echo $device['device_code']; ?></a></td>
                    <td><?php echo $device['address'] ?></td>
                    <td><?php echo \SanwoPHPAdapter\Globals\ServiceConstant::getStatus($device['status']); ?></td>
                    <td>
                        <div class="btn-group-xs">
                            <?php
                            if(in_array($device['status'],[\SanwoPHPAdapter\Globals\ServiceConstant::STATUS_DEVICE_ASSIGNED, \SanwoPHPAdapter\Globals\ServiceConstant::STATUS_DEVICE_ONLINE]))
                            {
                                echo "<a class='btn btn-danger '>Deactivate</a>";
                                echo "<a href='/home/transactions/".$device['id']."/5'>Transactions</a>";


                            }else {
                                if($device['status'] == \SanwoPHPAdapter\Globals\ServiceConstant::STATUS_DEVICE_UNASSIGNED){
                                    echo "<a class='btn btn-default'>Assign Device</a>";
                                }else {
                                    echo "<a class='btn btn-success'>Request For Activation</a>";
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>
                <?php
                }
            }
            ?>
            </tbody>
    </table>
    <div class="row table_paging">
        <div class="col-sm-6">
            <div class="dataTables_info">Showing 21 to 30 of 57 entries</div>
        </div>
        <div class="col-sm-6">
            <div class="dataTables_paginate paging_simple_numbers" id="editable_paginate">
                <ul class="pagination">
                    <li class="paginate_button previous"tabindex="0" id="editable_previous"><a href="#">Previous</a></li>
                    <li class="paginate_button " tabindex="0">
                        <a href="#">1</a>
                    </li>
                    <li class="paginate_button " tabindex="0">
                        <a href="#">2</a>
                    </li>
                    <li class="paginate_button active" tabindex="0">
                        <a href="#">3</a>
                    </li>
                    <li class="paginate_button " tabindex="0">
                        <a href="#">4</a>
                    </li>
                    <li class="paginate_button " tabindex="0">
                        <a href="#">5</a>
                    </li>
                    <li class="paginate_button " tabindex="0">
                        <a href="#">6</a>
                    </li>
                    <li class="paginate_button next" tabindex="0" id="editable_next">
                        <a href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

    </div>
    </div>
    </div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>

