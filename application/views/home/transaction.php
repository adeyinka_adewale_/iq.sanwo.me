<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
if(!empty($template) && !empty($transactions)){
    Calypso::AddPartialView($template, ['transactions' => $transactions]);
}else {
    Calypso::getInstance()->setFlashErrorMsg('Report Not Available This Time. Please try again later.');
   // echo '<h3>Report Not Available This Time. Please try again later.</h3>';
    Calypso::AddPartialView('notices');
}
?>
