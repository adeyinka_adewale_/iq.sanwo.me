<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<?php
Calypso::AddPartialView('_addDeviceToMerchantModal',['devices'=> !empty($all_devices)? $all_devices:[]]);
Calypso::AddPartialView('_addCashierModal');
Calypso::AddPartialView('notices');

if(!empty($all_devices)){
    //var_dump($all_devices);
}
if(!empty($cashiers)){
    //var_dump($cashiers);
}
if(!empty($devices)){
    //var_dump($devices);
}
?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo BASE_PATH; ?>/home/merchants">Merchants</a>
            </li>
            <li class="active">
                <strong>Merchant Details</strong>
            </li>
        </ol>
    </div>
</div>
<div class="btn-group-md">
<?php
    if(!empty($merchant)): ?>
    <a class="btn btn-primary" href="<?php echo BASE_PATH; ?>/home/merchant_transactions/<?php echo $merchant['id'] ?>">
    View Transactions
    </a>
    <?php endif; ?>
    <button data-toggle="modal" data-target="#add_cashier" class="btn btn-warning pull-right" style="margin-left:5px;">Add Cashier
    </button>
    <a  data-toggle="modal" data-target="#add_device_merchant" class="btn btn-success pull-right">Assign Device
    </a>
    <a href="<?php echo BASE_PATH; ?>/home/merchantdetail/<?php echo $merchant['id'] ?>" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</div>
<div class="row" style="display: block;">
    <div class="col-md-6">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Merchant Detail</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline noFilter">
    <?php
         if(!empty($merchant)): ?>
    <table class="table table-bordered table-hover dataTable" id="editable" role="grid" aria-describedby="editable_info">
     <thead>
        <tr>
            <th>Merchant name</th>
            <th><?= $merchant['name']; ?></th>
        </tr>
        <tr>
            <th>Merchant Phone Number</th>
            <th><?= $merchant['phone_number']; ?></th>
        </tr>
        <tr>
            <th>Merchant Address</th>
            <th><?= $merchant['address']; ?></th>
        </tr>

        <tr>
            <th>Merchant bank</th>
            <th><?= $merchant['bank']; ?></th>
        </tr>
        <tr>
            <th>Merchant Account Name</th>
            <th><?= $merchant['account_name']; ?></th>
        </tr>
        <tr>
            <th>Merchant Account Number</th>
            <th><?= $merchant['account_number']; ?></th>
        </tr>
        <tr>
            <th>Merchant Charge</th>
            <th>&#8358;&nbsp;<?= $merchant['charge']; ?></th>
        </tr>
        <tr>
            <th style="padding-top:15px;">Actions</th>
            <th><button class="btn btn-primary" style="margin-bottom:0px;">Edit</button></th>
        </tr>  
        </thead>
    </table>
<?php endif; ?>
</div>

    </div>
    </div>
    </div>
    <div class="col-md-6">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Devices</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline noFilter">
    <table class="table table-bordered table-hover  dataTable" id="editable __devices" role="grid" aria-describedby="editable_info">
     <thead>
        <tr>
            <th>#</th>
            <th>Device Code</th>
            <th>Device Address</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php  if(!empty($devices['data'])):
            $i = 0;
            foreach($devices['data'] as $device):
                ?>
                <tr>
                    <td><?=  ++$i; ?></td>
                    <td><?=  $device['device_code']; ?></td>
                    <td><?=  $device['address']; ?></td>
                    <td><button class="btn btn-primary">Edit</button></td>
                </tr>
                <?php
            endforeach;
        endif; ?>

        </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

    <div class="row" style="display: block;">
    <div class="col-md-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Cashiers</h5>
        <div class="ibox-tools">            
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <?php
        if(!empty($cashiers)) {
    ?>
    <table class="table table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr>
        <th>#</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Date Created</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach($cashiers['data'] as $item){

        ?>
        <tr>
            <td><?=  ++$i;?></td>
            <td><?= $item['email']; ?></td>
            <td><?= $item['telephone']; ?></td>
            <td><?= $item['created_time']; ?></td>
            <td><button class="btn btn-primary">Edit</button></td>
        </tr>

        <?php
    }
    ?></tbody>
</table>
<?php

}
?>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>