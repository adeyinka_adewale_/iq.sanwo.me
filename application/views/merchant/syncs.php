<?php
Calypso::AddPartialView('notices');


?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<div class="row">
<div class="col-sm-6">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo BASE_PATH; ?>/road/index">Roads</a>
            </li>
            <li class="active">
                <strong> Sync.</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-6"> 
<?php
if(Calypso::getInstance()->isAdmin()) {
    ?> 
    <div class="">  
        <a class="btn btn-warning" href="<?php echo BASE_PATH; ?>/merchant/transactions/<?php echo $merchant['id'] ?>" class="btn btn-primary" style="float:right; margin-left:10px;">View all Transactions</a>
        <a class="btn btn-success" href="<?php echo BASE_PATH; ?>/merchant/details/<?php echo $merchant['id'] ?>" class="btn btn-primary" style="float:right;">Road Detail</a>
    </div>
    <?php
}
?>
</div>
</div>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Synchronisations</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr class="row">
    <td>
            <h4>Search By Date:</h4>

    </td>

    <td>
    <input  class="form-control" type="date" placeholder='From' />
    </td>

     <td>
    <input  class="form-control" type="date" placeholder='To' />
    </td>

    </tr>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync Date</th>
        <th tabindex="0" rowspan="1" colspan="1">Total Amount</th>
        <th tabindex="0" rowspan="1" colspan="1">No. of Transactions</th>
       
        <th tabindex="0" rowspan="1" colspan="1">Actions</th>
    </tr>
    </thead>
    <tbody>
            <?php
            if(!empty($sync_transactions)) {
                $x = 0;
                foreach($sync_transactions as $transaction){

                ?>
                <tr>
                  <td><?php echo ++$x; ?></td>
                    <td>
                        <?= $transaction['created_time']?>
                    </td>
                     <td>
                         <?= $transaction['total_amount']?>
                    </td>
                    <td>
                         <?= $transaction['no_of_transaction']?>
                    </td>
                   
                   
                    
                    <td>
                    
                        <a href="<?php echo BASE_PATH; ?>/merchant/transactions/<?php echo $merchant['id']. '/'.$transaction['id']  ?>" class="btn btn-primary btn-danger btn-xs">View All</a>
                    </td>
                </tr>
                <?php

                }
            }
            ?>
            </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
