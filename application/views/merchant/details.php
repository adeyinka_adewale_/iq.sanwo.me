<?php
Calypso::AddPartialView('_addDeviceToMerchantModal',['devices'=> !empty($all_devices)? $all_devices:[]]);
Calypso::AddPartialView('_addCashierModal');
Calypso::AddPartialView('notices');

if(!empty($all_devices)){
    //var_dump($all_devices);
}
if(!empty($cashiers)){
    //var_dump($cashiers);
}
if(!empty($devices)){
    //var_dump($devices);
}
?>
<div class="row">
<div class="col-lg-12">
<?php
    if(!empty($merchant)): ?>
    <a class="btn btn-primary btn-rounded" href="<?php echo BASE_PATH; ?>/merchant/transactions/?id=<?php echo $merchant['id'];?>">
    View Transactions
    </a>
    <?php endif; ?>
    <button data-toggle="modal" data-target="#add_cashier" class="btn btn-warning pull-right" style="margin-left:10px;">Add Cashier
    </button>
    <a  data-toggle="modal" data-target="#add_device_merchant" class="btn btn-success pull-right">Assign Device
    </a>
</div>
</div>
<div class="row">             
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Merchant Details</h5>
            </div>
            <div class="ibox-content">
                 <div id="editable_wrapper" class="dataTables_wrapper form-inline" style="padding-bottom:0px;">                 
                <table class="table table-bordered" style="margin-bottom:0px;">
                    <thead>
                    <tr>
                        <th>Merchant name</th>
                        <th><?php
                                echo $merchant['name']
                         ?></th>
                    </tr>
                    <tr>
                        <th>Merchant Address</th>
                        <th>
                            <?php
                                echo $merchant['address']
                         ?>
                        </th>
                    </tr>
                    <tr>
                        <th>Merchant Phone Number</th>
                        <th>08362627383</th>
                    </tr>
                    <tr>
                        <th>No of Merchant Devices</th>
                        <th>4</th>
                    </tr>
                    <tr>
                        <th>No of Parking Attendants</th>
                        <th>4</th>
                    </tr>
                    <tr>
                        <th>Merchant Charge</th>
                        <th>&#8358;&nbsp;1.5%</th>
                    </tr>

                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Registered Devices</h5>
            </div>
            <div class="ibox-content">
                 <div id="editable_wrapper" class="dataTables_wrapper form-inline" style="padding-bottom:0px;"> 
                <table id="__devices" class="table table-bordered" style="margin-bottom:0px;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Device Serial</th>
                        <th>Device Location</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>KKLD83930</td>
                            <td>Okelowo, Abeokuta</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>JKE9304090</td>
                            <td>Okelewo, Sango</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>List of Parking Attendants</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" id="trans">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">Parking Attendant</th>
        <th tabindex="0" rowspan="1" colspan="1">Attendant Location</th>
        <th tabindex="0" rowspan="1" colspan="1">Phonenumber</th>
        <th tabindex="0" rowspan="1" colspan="1">No of Transactions</th>  
        <th tabindex="0" rowspan="1" colspan="1">Amount Processed</th>      
        <th tabindex="0" rowspan="1" colspan="1">Trans. Time</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync. Time</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>Afolabi Olamide</td>
            <td>Okelowo</td>
            <td>080553056933</td>
            <td>50</td>
            <td>5000</td>
            <td>16/12/2015 8:00am</td>
            <td>16/12/2015 8:00am</td>
        </tr>    
         <tr>
            <td>Samuel Popoola</td>

            <td>Okelowo</td>
            <td>070233056943</td>
            <td>10</td>
            <td>1000</td>
            <td>17/12/2015 8:00am</td>
            <td>17/12/2015 8:15am</td>
        </tr>    
         <tr>
            <td>Ita Ukeme</td>
            <td>Okelowo</td>
            <td>080653054113</td>
            <td>20</td>
            <td>2000</td>
            <td>16/12/2015 8:00am</td>
            <td>16/12/2015 8:00am</td>
        </tr>    
         <tr>
            <td>Segun Ayeni</td>
            <td>Okelowo</td>
            <td>070443355674</td>
            <td>50</td>
            <td>50000</td>
            <td>16/12/2015 8:00am</td>
            <td>16/12/2015 8:00am</td>
        </tr>        
    </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>List of Transactions (From Most Recent)</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content" id="trans">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">Plate Number</th>
        <th tabindex="0" rowspan="1" colspan="1">Card Balance</th>
        <th tabindex="0" rowspan="1" colspan="1">Time Purchased</th>        
        <th tabindex="0" rowspan="1" colspan="1">Trans. Time</th>
        <th tabindex="0" rowspan="1" colspan="1">Sync. Time</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>OG7867AB</td>
            <td>400</td>
            <td>4 hours</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
            <td>OG7867AB</td>
            <td>1000</td>
            <td>10 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
        <tr>
             <td>OG7867AB</td>
            <td>100</td>
            <td>1 hour</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
             <td>OG7867AB</td>
            <td>1250</td>
            <td>4 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
        <tr>
            <td>OG7867AB</td>
            <td>200</td>
            <td>2 hours</td>
            <td>7:55am 15/12/2015</td>
            <td>9:00pm 15/12/2015</td>
        </tr>
        <tr>
            <td>OG7867AB</td>
            <td>300</td>
            <td>3 hours</td>
            <td>3:55am 14/12/2015</td>
            <td>9:00pm 14/12/2015</td>
        </tr>
    </tbody>
    </table>
</div>
</div>
</div>
</div>
</div>
