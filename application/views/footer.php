</div>
<div class="footer">
            <div class="pull-right">
                <strong><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?></strong>
            </div>
            <div>
                <strong>Copyright</strong> <?php echo $session['email']; ?> &copy; 2015
            </div>
        </div>
</div>
</div>
<script src="<?php echo BASE_PATH; ?>/js/datepicker.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/jquery-2.1.1.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/jquery-ui-1.10.4.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/inspinia.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/jeditable/jquery.jeditable.js"></script>

<!-- Data Tables -->
<script src="<?php echo BASE_PATH; ?>/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/dataTables/dataTables.responsive.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/plugins/dataTables/dataTables.tableTools.min.js"></script>

<!-- ChartJS-->
<script src="<?php echo BASE_PATH; ?>/js/plugins/chartJs/Chart.min.js"></script>
<script src="<?php echo BASE_PATH; ?>/js/demo/chartjs-demo.js"></script> 
<script type="text/javascript">
        var minutesLabel = document.getElementById("minutes");
        var secondsLabel = document.getElementById("seconds");
        var totalSeconds = 2102;
        setInterval(setTime, 1000);

        function setTime()
        {
            ++totalSeconds;
            secondsLabel.innerHTML = pad(totalSeconds%60);
            minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
        }

        function pad(val)
        {
            var valString = val + "";
            if(valString.length < 2)
            {
                return "0" + valString;
            }
            else
            {
                return valString;
            }
        }
    </script>
<script type="text/javascript">
    function receiveAsyncResponse(data) {
        if(typeof data.callback != "undefined") {
            var fn = getFunctionFromString(data.callback);
            fn(data.data);
        }
    }
    window.getFunctionFromString = function(string)
    {
        var scope = window;
        var scopeSplit = string.split('.');
        for (i = 0; i < scopeSplit.length - 1; i++)
        {
            scope = scope[scopeSplit[i]];

            if (scope == undefined) return;
        }

        return scope[scopeSplit[scopeSplit.length - 1]];
    }
    $(document).ready(function(){
        var timelineBlocks = $('.cd-timeline-block'),
            offset = 0.8;

        //hide timeline blocks which are outside the viewport
        hideBlocks(timelineBlocks, offset);

        //on scolling, show/animate timeline blocks when enter the viewport
        $(window).on('scroll', function(){
            (!window.requestAnimationFrame)
                ? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
                : window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
        });

        function hideBlocks(blocks, offset) {
            blocks.each(function(){
                ( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
            });
        }

        function showBlocks(blocks, offset) {
            blocks.each(function(){
                ( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
            });
        }


        $('.close-carbon-adv').on('click', function(event){
            event.preventDefault();
            $('#carbonads-container').hide();
        });

        $('#select-what').selectpicker({title:'What?'});
        $('#select-where').selectpicker({title:'Where?'});
        $('.selectpicker').selectpicker();


    });
</script>
<div class="xfooter-banner">

</div>
<?php
if(Calypso::getInstance()->isLoggedIn()) {
    $categories = empty($categories)?[]:$categories;
    Calypso::getInstance()->AddPartialView('create_events',['categories'=> $categories]);
}
?>
<iframe name="ajax_form_frame" id="ajax_form_frame" style="display: none;top: -9999px; left: -9999px;"></iframe>
</body>

</html>