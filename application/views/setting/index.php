<?php
Calypso::AddPartialView('_addMerchantModal');
Calypso::AddPartialView('notices');


?>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>/js/jquery.dataTables.min.js"></script>
<h1>Registered Merchants
    <a href="<?php echo BASE_PATH; ?>/home/merchants" class="refresh">
        <i class="fa fa-refresh"></i>
    </a>
</h1>
<?php
if(Calypso::getInstance()->isAdmin()) {
    ?>
    
    <button type="button" class="btn btn-success pull-right evt-margin-top-x50-neg" data-toggle="modal"
            data-target="#add_merchant">Add New Merchant
    </button>
    <?php
}
?>
<div class="row" style="display: block;">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?php echo empty($session['issuer'])?'Sanwo Client': strtoupper($session['issuer']['name']); ?>&nbsp;Merchants</h5>
        <div class="ibox-tools">
            <span class="label label-primary">Last Updated:&nbsp;<span id="date"></span><script>document.getElementById("date").innerHTML = Date();</script></span>
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="ibox-content">
    <div id="editable_wrapper" class="dataTables_wrapper form-inline">
    <table class="table table-striped table-bordered table-hover  dataTable" id="editable" role="grid" aria-describedby="editable_info">
    <thead>
    <tr role="row">
        <th tabindex="0" rowspan="1" colspan="1">#</th>
        <th tabindex="0" rowspan="1" colspan="1">Name</th>
        <th tabindex="0" rowspan="1" colspan="1">Phone</th>
        <th tabindex="0" rowspan="1" colspan="1">Charge</th>
        <th tabindex="0" rowspan="1" colspan="1">Account Name</th>
        <th tabindex="0" rowspan="1" colspan="1">Bank</th>
        <th tabindex="0" rowspan="1" colspan="1">Account Number</th>
        <th tabindex="0" rowspan="1" colspan="1">Address</th>
        <th tabindex="0" rowspan="1" colspan="1">Action</th>
    </tr>
    </thead>
    <tbody>
            <?php
            if(!empty($merchants['data'])) {
                $x = 0;
                foreach($merchants['data'] as $merchant){

                ?>
                <tr>
                    <td><?php echo ++$x; ?></td>
                    <td><?php echo ucwords($merchant['name']); ?></td>
                    <td><?php echo $merchant['phone_number']; ?></td>
                    <td><?php echo $merchant['charge']; ?></td>
                    <td><?php echo $merchant['account_name']; ?></td>
                    <td><?php echo $merchant['bank'] ?></td>
                    <td><?php echo $merchant['account_number'] ?></td>
                    <td><?php echo $merchant['address'] ?></td>
                    <td><a href="<?php echo BASE_PATH; ?>/home/merchantdetail/<?php echo $merchant['id'] ?>">Detail</a></td>
                </tr>
                <?php

                }
            }
            ?>
            </tbody>
    </table>
</div>

    </div>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('table').DataTable();
    });
</script>
