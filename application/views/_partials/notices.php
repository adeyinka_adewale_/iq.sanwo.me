<?php
$msg = Calypso::getInstance()->flashErrorMsg();
$msg_success = Calypso::getInstance()->flashSuccessMsg();
if($msg):
    ?>
    <div class="alert alert-danger ">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo $msg; ?>
    </div>
<?php
endif;
if($msg_success):
    ?>
    <div class="alert alert-success ">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <?php echo $msg_success; ?>
    </div>
<?php
endif;
?>