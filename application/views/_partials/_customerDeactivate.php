<div  class="modal fade inmodal in" id="modalDeactivate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Deactivate Customer</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger center"><h2>Are you sure you want to deactivate this customer?</h2></div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">No</button>
                    <button data-toggle="modal" data-target="#Deactivated" class="btn btn-success" data-dismiss="modal">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div  class="modal fade inmodal in" id="Deactivated" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Deactivate Customer</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info center"><h1>Deactivation was Successful!</h1></div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>