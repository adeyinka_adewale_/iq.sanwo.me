<div  class="modal fade inmodal in" id="add_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">New Customer</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="form-group">
                            <label for="inputPlate" class="col-lg-2 control-label">
                                Card Serial 
                            </label>
                            <div class="col-lg-10">
                                <input name="card_serial" type="text" class="form-control" id="" placeholder="Card Serial">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputPlate" class="col-lg-2 control-label">Plate No</label>
                            <div class="col-lg-10">
                                <input name="Plate" type="text" class="form-control" id="inputPlate" placeholder="Plate">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputTelephone" class="col-lg-2 control-label">Phone Number</label>
                            <div class="col-lg-10">
                                <input name="telephone" type="text" class="form-control" id="" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputPlate" class="col-lg-2 control-label">Full Name</label>
                            <div class="col-lg-10">
                                <input name="name" type="text" class="form-control" id="" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputPlate" class="col-lg-2 control-label">Vehicle Color</label>
                            <div class="col-lg-10">
                                <input name="color" type="text" class="form-control" id="" placeholder="Vehicle Color">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputPlate" class="col-lg-2 control-label">Vehicle Brand</label>
                            <div class="col-lg-10">
                                <input name="color" type="text" class="form-control" id="" placeholder="Vehicle Brand">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                <!-- <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span> -->
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Gender</label>
                            <div class="col-lg-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" id="gender" value="1" checked="">
                                        Male
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="gender" id="gender" value="2">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default" data-toggle="modal" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#Success" data-dismiss="modal">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div  class="modal fade inmodal in" id="Success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Add Customer</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info center"><h1>Customer was added Successfully!</h1></div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>