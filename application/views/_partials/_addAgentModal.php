
<div  class="modal fade inmodal in" id="add_agent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">New Agent</h4>
            </div>
            <div class="modal-body">
                <p>
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
                    <fieldset class="">
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Agent Name</label>
                            <div class="col-lg-10">
                                <input name="name" type="text" class="form-control" id="" placeholder="Store/Agent Name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="address" name="address"></textarea>
                                <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="bank">Bank</label>
                            <div class="col-lg-10">
                                <select id="bank" class="form-control" name="bank">
                                    <option value="Access Bank">Access Bank</option>
                                    <option value="Citibank">Citibank</option>
                                    <option value="Diamond Bank">Diamond Bank</option>
                                    <option value="Ecobank Nigeria">Ecobank Nigeria</option>
                                    <option value="Enterprise Bank Limited">Enterprise Bank Limited</option>
                                    <option value="Fidelity Bank Nigeria">Fidelity Bank Nigeria</option>
                                    <option value="First Bank of Nigeria">First Bank of Nigeria</option>
                                    <option value="First City Monument Bank">First City Monument Bank</option>
                                    <option value="Guaranty Trust Bank">Guaranty Trust Bank</option>
                                    <option value="Heritage Bank Plc">Heritage Bank Plc</option>
                                    <option value="Keystone Bank Limited">Keystone Bank Limited</option>
                                    <option value="Mainstreet Bank Limited">Mainstreet Bank Limited</option>
                                    <option value="Rand Agent Bank">Rand Agent Bank</option>
                                    <option value="Savannah Bank">Savannah Bank</option>
                                    <option value="Skye Bank">Skye Bank</option>
                                    <option value="Stanbic IBTC Bank Nigeria Limited">Stanbic IBTC Bank Nigeria Limited</option>
                                    <option value="Standard Chartered Bank">Standard Chartered Bank</option>
                                    <option value="Sterling Bank">Sterling Bank</option>
                                    <option value="Union Bank of Nigeria">Union Bank of Nigeria</option>
                                    <option value="United Bank for Africa">United Bank for Africa</option>
                                    <option value="Unity Bank Plc">Unity Bank Plc</option>
                                    <option value="Wema Bank">Wema Bank</option>
                                    <option value="Zenith Bank">Zenith Bank</option>
                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="account_name">Agent Account Name</label>
                            <div class="col-lg-10">
                                <input type="text" id="account_name" class="form-control" name="account_name">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="account_number">Agent Account Number</label>
                            <div class="col-lg-10">
                                <input type="text" id="account_number" class="form-control" name="account_number">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="charge_type">Agent Charge Type</label>
                            <div class="col-lg-10">
                                <select id="charge_type" class="form-control" name="charge_type">
                                    <option value="1">Absolute</option>
                                    <option value="2">Percentage</option>
                                </select>

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="control-label col-lg-2" for="charge">Agent Charge</label>
                            <div class="col-lg-10">
                                <input type="text" id="charge" class="form-control" name="charge">

                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Telephone</label>
                            <div class="col-lg-10">
                                <input name="telephone" type="text" class="form-control" id="" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input name="email" type="text" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Password</label>
                            <div class="col-lg-10">
                                <input name="password" type="text" class="form-control" id="" placeholder="password">
                            </div>
                        </div>                        
                    </fieldset>
                </form>
                </p>
            </div>
        <div class="modal-footer">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="reset" class="btn btn-default" data-toggle="modal" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#Success" data-dismiss="modal">Submit</button>
            </div>
        </div>
        </div>
    </div>
</div>

<div  class="modal fade inmodal in" id="Success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Add Agent</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info center"><h1>Agent was added Successfully!</h1></div>
            </div>
            <div class="modal-footer">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>