<div class="modal fade" id="reset_password_modal" tabindex="-1" role="dialog" modal-backdrop="static" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Contact Information</h4>
            </div>
            <form method="post" action="<?php echo BASE_PATH; ?>/home/changepassword/">
                <div id="customer_detail_body" class="modal-body">

                        <label>New Password</label>
                        <input class="form-control" name="pwd" type="password">
                        <label>Confirm New Password</label>
                        <input class="form-control" name="new_pwd" type="password">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary ">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>