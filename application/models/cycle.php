<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 11/19/2015
 * Time: 6:14 AM
 */
class Cycle extends VanillaModel{

    public function getCycles($issuer_id)
    {
        $this->where('issuer_id', $issuer_id);
        $this->where('status', 1);
        return $this->search();
    }
}