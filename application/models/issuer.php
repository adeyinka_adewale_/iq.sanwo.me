<?php
/**
 * Created by PhpStorm.
 * User: CodeBeast
 * Date: 10/24/2015
 * Time: 12:23 AM
 */
class Issuer extends VanillaModel{

	public $id;
	public $name;
	public $address;
	public $top_up_charge;


	public function set_id($id){
		$this->id = $id;
	}

	public function get_id(){
		return $this->$id;
	}

	public function set_name($name){
		$this->$name = $name;
	}

	public function get_name(){
		$return $this->name;
	}
}